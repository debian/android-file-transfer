android-file-transfer (4.3-1) unstable; urgency=medium

  * New upstream version 4.3
  * Drop patch included in upstream release
  * Do not repack upstream tarball, no longer needed
  * Standards-Version: 4.6.2 (no changes required)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 16 Jan 2024 09:50:15 +0100

android-file-transfer (4.2-2) unstable; urgency=medium

  * Cherry-pick upstream fix for GCC 13 (Closes: 1037572)
  * Drop useless debian/salsa-ci.yml

 -- Dylan Aïssi <daissi@debian.org>  Tue, 25 Jul 2023 11:44:39 +0200

android-file-transfer (4.2-1) unstable; urgency=medium

  * Update VCS fields to point to the Debian namespace on salsa.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add qttools5-dev to BD

 -- Dylan Aïssi <daissi@debian.org>  Tue, 02 Feb 2021 17:38:41 +0100

android-file-transfer (3.9+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Remove upstream patch.
  * Bump Standards-Version to 4.4.0.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 09 Sep 2019 21:33:30 +0200

android-file-transfer (3.7+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #928309)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 01 May 2019 22:08:46 +0200
